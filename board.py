from pprint import pprint
import sys

def is_safe_move(placements):
    row = len(placements)-1
    for i in range(0,row):
        diff = abs(placements[i] - placements[row])
        if diff == 0 or diff == (row - i):
            return False
    return True

def solve(n, row, col_placements, solutions):
    if row == n:
        solutions.append(col_placements.copy())
    else:
        for col in range(0, n):
            col_placements.append(col)
            if is_safe_move(col_placements):
                solve(n, row+1, col_placements, solutions)
            col_placements.pop()

def main(args):
    n = int(args[0])
    result = []
    solve(n, 0, [], result)
    pprint(result, width=40)

if __name__ == "__main__":
    main(sys.argv[1:])